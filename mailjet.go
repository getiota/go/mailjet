// Package mailjet wraps the mailjet API
package mailjet

import (
	"errors"
	"net/http"
	"net/url"
	"os"
	"path"
)

// Client wraps the API
type Client interface {
	CanSendEmail() error
	CanSendSMS() error

	Send(*Email) error
	SendSMS(text string, to string, from string) error
}

type defaultClient struct {
	key    string
	secret string
	token  string

	client http.Client
	root   url.URL
}

// NewClientFromEnv creates a client from env var MAILJET_KEY / MAILJET_SECRET / MAILJET_TOKEN
func NewClientFromEnv() Client {
	return NewClient(os.Getenv("MAILJET_KEY"), os.Getenv("MAILJET_SECRET"), os.Getenv("MAILJET_TOKEN"))
}

// NewClient is the default constructor
func NewClient(key string, secret string, token string) Client {
	u, _ := url.Parse("https://api.mailjet.com")
	return &defaultClient{key: key, secret: secret, token: token, root: *u}
}

func (c *defaultClient) url(p string) string {
	url := c.root
	url.Path = path.Join(url.Path, p)
	return url.String()
}

func (c *defaultClient) CanSendEmail() error {
	if c.key == "" {
		return errors.New("missing mailjet key")
	}
	if c.secret == "" {
		return errors.New("missing mailjet secret")
	}
	return nil
}

func (c *defaultClient) CanSendSMS() error {
	if c.token == "" {
		return errors.New("missing mailjet token")
	}
	return nil
}
