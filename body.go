package mailjet

// Body is a mailjet object
type body struct {
	Messages []Email
}

// NewEmail creates an email
func NewEmail(template int, email string, values map[string]string) *Email {
	return &Email{
		To:               []Recipient{{Email: email}},
		TemplateID:       template,
		Variables:        values,
		TemplateLanguage: true,
	}
}

// Email is a mailjet object
type Email struct {
	To               []Recipient
	TemplateID       int
	TemplateLanguage bool
	Variables        map[string]string
	TrackClicks      Option
	Attachments      []Attachment `json:"Attachments,omitempty"`
}

// Recipient is a mailjet object
type Recipient struct {
	Email string
	Name  string `json:"Name,omitempty"`
}

// Option is a bool with a default value
type Option string

// An enum of options
const (
	OptionEnabled  Option = "enabled"
	OptionDisabled Option = "disabled"
	OptionDefault  Option = "account_default"
)

// Attachment allows attaching files to emails
type Attachment struct {
	ContentType   string
	Filename      string
	Base64Content string
}
