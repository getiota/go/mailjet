# Pages

A simple utility to provide paginations for echo micro services

## Usage

```go
// Connector wraps the database
type Connector interface {
	ListUsers(int, int) ([]models.User, error)
	CountUsers() (int64, error)
}

func List(connector Connector) echo.HandlerFunc {
	return func(c echo.Context) error {
		page, err := pages.Make(
			c,
			func(offset int, limit int) (interface{}, error) {
				return connector.ListUsers(offset, limit)
			},
			connector.CountUsers,
		)

		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, page)
	}
}
```
