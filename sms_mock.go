// +build no_mailjet

package mailjet

import "github.com/rs/zerolog/log"

func (c *defaultClient) SendSMS(text string, to string, from string) error {
	log.Info().Msgf("Did not send sms '%s' to '%s'", text, to)
	return nil
}
