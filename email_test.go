package mailjet

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSendRegisterEmail(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(http.StatusOK)
		body, _ := ioutil.ReadAll(req.Body)
		defer req.Body.Close()

		assert.Equal(
			t,
			`{"Messages":[{"To":[{"Email":"somebody@example.com"}],"TemplateID":1777307,"TemplateLanguage":true,"Variables":{"code":"12345","uuid":"https://google.com"},"TrackClicks":"enabled"}]}`, //nolint:lll
			string(body),
		)

		assert.Equal(t, "Basic aGVsbG86Z29vZGJ5ZQ==", req.Header.Get("Authorization"))
	}))
	defer func() { testServer.Close() }()

	u, _ := url.Parse(testServer.URL)
	client := defaultClient{"hello", "goodbye", "", *testServer.Client(), *u}
	values := make(map[string]string)
	values["uuid"] = "https://google.com"
	values["code"] = "12345"
	e := NewEmail(1777307, "somebody@example.com", values)
	e.TrackClicks = OptionEnabled
	err := client.Send(e)
	assert.Nil(t, err)
}
