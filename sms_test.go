package mailjet

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSendSMS(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(http.StatusOK)
		body, _ := ioutil.ReadAll(req.Body)
		defer req.Body.Close()

		assert.Equal(
			t,
			`{"From":"Hello","Text":"Have a nice SMS flight with Mailjet !","To":"+33600000000"}`,
			string(body),
		)

		assert.Equal(t, req.Header.Get("Authorization"), "Bearer imatoken")
	}))
	defer func() { testServer.Close() }()

	u, _ := url.Parse(testServer.URL)
	client := defaultClient{"", "", "imatoken", *testServer.Client(), *u}

	err := client.SendSMS("Have a nice SMS flight with Mailjet !", "+33600000000", "Hello")
	assert.Nil(t, err)
}
