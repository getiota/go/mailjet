// +build no_mailjet

package mailjet

import "github.com/rs/zerolog/log"

func (c *defaultClient) Send(e *Email) error {
	log.Info().Msgf("Did not send email to %s with values %s", e.To, e.Variables)
	return nil
}
